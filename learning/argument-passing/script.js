//--------------------------------------------------------------------------------------------------
// JavaScript - argument passing
// Author: gitlab.com/fejak
//--------------------------------------------------------------------------------------------------
"use strict";
//--------------------------------------------------------------------------------------------------
// a is a copy of the original reference
// it points to the original location but it isn't the original reference (just like in Python)
function f2 ( obj )
{
    // we can add a new member to the referenced object
    obj . x = 5;

    // we can reassign the reference here, but it's only a local change
    // (also there's no way how to make a function input argument "const")
    obj = {};

    // we're modifying the local object, the change won't affect the original object
    obj . y = 10;
}
//--------------------------------------------------------------------------------------------------
// JS strings are "primitive" (it's a singular value, not an object with members)
// all primitives in JS are "immutable" (including numbers - why?)
// (why are strings primitives at all?)
//
// str is passed "by value" but what probably really happens "under the hood" (in the JS engine)
// is that it's passed by reference (otherwise it would be horribly inefficient)
// https://stackoverflow.com/questions/1308624
// https://stackoverflow.com/questions/12424150
function f3 ( str )
{
    // but we can seemingly access members on primitives (!)
    // the primitive value is implicitly converted to its wrapper object which is then thrown away
    // basically what happens is: new String ( str ) . length
    console . log ( str . length );
    // in JS we can also access object members this way (objects are dictionaries)
    console . log ( str ["length"] );

    // error - strings are immutable
    // str [0] = 'x';
}
//--------------------------------------------------------------------------------------------------
function f1 ()
{
    // create a const reference to a newly created object
    const a = {};
    // error - we cannot reassign a constant
    // a = {};

    // a is empty
    console . log ( a );
    f2 ( a );
    // a contains only the x member
    console . log ( a );

    let str = "abcd";
    // OK - we can reassign a variable declared with "let"
    str = "xxx";
    f3 ( str ); // the length of the string is 3
}
//--------------------------------------------------------------------------------------------------
f1 ();
