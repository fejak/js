//--------------------------------------------------------------------------------------------------
// The basics of JavaScript (JS)
// Author: gitlab.com/fejak
//--------------------------------------------------------------------------------------------------
/*
    Basic facts:
        - JS is typically used for client-side scripting in web browsers - the local manipulation
          of an already downloaded and parsed website (a HTML document).
        - JS uses the C-like syntax but otherwise it's a completely different language.
        - JS uses the prototype-based OOP model (there are no classes, just objects, some of which
          are "prototypes" - meant for building another objects). All objects (and prototypes)
          can be modified at runtime. There are built-in objects (and prototypes) for every instance
          of a JS program - also these built-in objects can be modified at runtime, which is not
          recommended.
          (why does JS have no mechanism for protection of built-in objects?)
        - JS is dynamically typed (variables can be freely reassigned to data objects of various
          types).
        - JS is a dynamic language (the structure of a JS program can be changed during the
          execution) - like in e.g. Python and unlike in e.g. C/C++.
        - JS is very weakly typed - the interpreter always tries to evaluate the expressions
          somehow instead of throwing an error.
        - Modern implementations of JS are very fast and effective (typically the Google's V8
          engine) - JS typically runs much faster then e.g. Python.
        - JS was never meant for the scope of usage it is used these days, so originally it was a
          small and simple language with many flaws.
        - The author of JS, Brendan Eich, allegedly had to design the language in about 10 days (!).
        - The imperfection of JS is the reason why there are many various JS libraries and
          frameworks meant to make the development more effective and safer.
        - JS has many weird quirks and features the programmer has to get used to.
        - Today JS with its frameworks is one of the most popular languages in the world.
*/
//--------------------------------------------------------------------------------------------------
/*
    Short history:
        1995
            1st version (as a part of the Netscape Navigator)
        1997
            ECMAScript 1
        1998
            ECMAScript 2
        1999
            ECMAScript 3
                === and !== operators for comparison (non-coercing - the type is preserved)
        2005
            Ajax (XMLHttpRequest - first used in MS Access web app in late 90s)
                this led to JS renaissance
                many new libraries were created (jQuery etc...)
        2008
            the V8 engine from Google, using the JIT (just-in-time) compilation
        2009
            ECMAScript 5
                "use strict"
            NodeJS
        2015
            ECMAScript 6 (ECMAScript 2015) - many new features
                let, const
                arrow function
                classes (just a syntax sugar for the underlying prototypal inheritance)
                promises
                etc...
*/
//--------------------------------------------------------------------------------------------------
// this declaration enforces the "strict mode"
// every variable must be declared before its usage (using the var / let / const keywords)
"use strict";
//--------------------------------------------------------------------------------------------------
// this is the so-called "function declaration"
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/function
function f2 ()
{
    {
        var x = 5;
        let y = 10;
        const z = 20;
    }
    // x is visible in the whole function (var is function-scoped)
    console . log ( x );
    // console is a part of the global "window" object
    // this object is implicit, so we don't have to refer it
    // window . console . log ( x ); // this is also OK
    // "window" is the top level object in the BOM (Browser Object Model - today also called WebAPI)

    // y and z are not visible outside of their scope (let and const are block-scoped)
    // console . log ( x );
    // console . log ( y );
}
//--------------------------------------------------------------------------------------------------
function f1 ()
{
    // only without "use strict"
    // this variable becomes global (available in the whole program), but this function must be
    // executed in order to become so
    // a = 6;
}
//--------------------------------------------------------------------------------------------------
f1 ();
f2 ();
