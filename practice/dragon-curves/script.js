"use strict";
//------------------------------------------------------------------------------
/*
 * Draw dragon curves (https://en.wikipedia.org/wiki/Dragon_curve).
 * HTML5 / canvas version.
 * Draw pixel-perfect Dragon curve (direct image data input - no anti-aliasing).
 *
 * Author: gitlab.com/fejak
 *
 * Version 0.1
 */
//------------------------------------------------------------------------------
// global variables
let g_yLen  = 1000; // can be changed
let g_xLen  = 1000; // can be changed

let g_i = 0; // current iteration of curve drawing, for animation

const g_yInit = g_yLen / 2;     // init positions
const g_xInit = g_xLen / 2;

let g_yPos = g_yInit;
let g_xPos = g_xInit;

let g_Running = false;
let g_Finished = false;

let g_intervals = [];
const g_intervalCnt = 10; // the greater, the more "parallel" execution

const g_dirs = {
    "left":     0,
    "right":    1,
    "up":       2,
    "down":     3
};
const g_initDir = g_dirs . right;
let g_curDir = g_initDir;

const g_Colors = { "red":    [255, 0, 0],
                   "blue":   [0, 0, 255],
                   "green":  [0, 255, 0],
                   "yellow": [255, 255, 0],
                   "black":  [ 0, 0, 0 ] };

const MAIN_COLOR = g_Colors . red;

const ITER = 17; // number of iterations

// stroke length ("unit length")
const g_StrokeLen = 2;

let g_initTime = null;
let g_wasPaused = false;
//------------------------------------------------------------------------------
function createCanvas ( g_yLen, g_xLen )
{
    const cnvs = document . createElement ( "canvas" );
    cnvs . id = "dragonCanvas";
    cnvs . height = g_yLen;
    cnvs . width  = g_xLen;
    cnvs . style . border = "1px solid #999";
    return cnvs;
}
//------------------------------------------------------------------------------
function createButton ( id, innerHTML, func )
{
    const btn = document . createElement ( "button" );
    btn . id = id;
    btn . innerHTML = innerHTML;
    btn . addEventListener ( "click", func );
    return btn;
}
//------------------------------------------------------------------------------
function createDiv ( id )
{
    const div = document . createElement ( "div" );
    div . id = id;
    return div;
}
//------------------------------------------------------------------------------
function createLayout ()
{
    const div1 = createDiv ( "div1" );
    const div2 = createDiv ( "div2" );

    const cnvs = createCanvas ( g_yLen, g_xLen );

    const btn1 = createButton ( "btnStart", "Start", start );
    const btn2 = createButton ( "btnSize500", "500 x 500", resizeCanvas500 );
    const btn3 = createButton ( "btnSize750", "750 x 750", resizeCanvas750 );
    const btn4 = createButton ( "btnSize1000", "1000 x 1000", resizeCanvas1000 );

    div1 . appendChild ( cnvs );
    div2 . appendChild ( btn1 );
    div2 . appendChild ( btn2 );
    div2 . appendChild ( btn3 );
    div2 . appendChild ( btn4 );

    document . body . appendChild ( div1 );
    document . body . appendChild ( div2 );
}
//------------------------------------------------------------------------------
function resizeCanvas ( size )
{
    const cnvs = document . getElementById ( "dragonCanvas" );
    g_yLen = g_xLen = size;
    cnvs . height = g_yLen;
    cnvs . width  = g_xLen;
    reset ();
}
//------------------------------------------------------------------------------
function resizeCanvas500 ()
{
    resizeCanvas ( 500 );
}
//------------------------------------------------------------------------------
function resizeCanvas750 ()
{
    resizeCanvas ( 750 );
}
//------------------------------------------------------------------------------
function resizeCanvas1000 ()
{
    resizeCanvas ( 1000 );
}
//------------------------------------------------------------------------------
function createButtons ()
{
    createButtonStart ();
    createButtonsCanvasSize ();
}
//------------------------------------------------------------------------------
function changePos ()
{
    switch ( g_curDir )
    {
        case g_dirs . left:   --g_xPos; break;
        case g_dirs . right:  ++g_xPos; break;
        case g_dirs . up:     --g_yPos; break;
        case g_dirs . down:   ++g_yPos; break;
    }
}
//------------------------------------------------------------------------------
function turnLeft ()
{
    switch ( g_curDir )
    {
        case g_dirs . left:   return g_dirs . down;
        case g_dirs . right:  return g_dirs . up;
        case g_dirs . up:     return g_dirs . left;
        case g_dirs . down:   return g_dirs . right;
    }
}
//------------------------------------------------------------------------------
function turnRight ()
{
    switch ( g_curDir )
    {
        case g_dirs . left:   return g_dirs . up;
        case g_dirs . right:  return g_dirs . down;
        case g_dirs . up:     return g_dirs . right;
        case g_dirs . down:   return g_dirs . left;
    }
}
//------------------------------------------------------------------------------
function changeDir ( curTurn )
{
    if ( curTurn === 'L' )
    {
        g_curDir = turnLeft ();
    }
    if ( curTurn === 'R' )
    {
        g_curDir = turnRight ();
    }
}
//------------------------------------------------------------------------------
function outOfDimensions ( g_yLen, g_xLen, g_yPos, g_xPos )
{
    return ( g_xPos < 0 || g_xPos >= g_xLen || g_yPos < 0 || g_yPos >= g_yLen );
}
//------------------------------------------------------------------------------
// color the current pixel
function colorPixel ( data, color )
{
    //const curPxl = ( g_yPos * cnvs . width + g_xPos ) * 4;
    data [0] = color [0];   // Red
    data [1] = color [1];   // Green
    data [2] = color [2];   // Blue
    data [3] = 255;         // Alpha
}
//------------------------------------------------------------------------------
function drawIter ( curve, color, ctx )
{
    if ( ! g_Running )
        return;

    const myImageData = new ImageData ( 1, 1 );

    for ( let j = 0; j < g_StrokeLen; ++j )
    {
        changePos ();

        if ( outOfDimensions ( g_yLen, g_xLen, g_yPos, g_xPos ) )
            continue;

        colorPixel ( myImageData . data, color );
        ctx . putImageData ( myImageData, g_xPos, g_yPos );
    }
    changeDir ( curve [g_i] );

    ++ g_i;

    // if end of the curve is reached
    if ( g_i === curve . length )
    {
        clearIntervals ();
        console . log ( "Done." );
        g_Finished = true;
    }
}
//------------------------------------------------------------------------------
function drawOnCanvasDataIter ( curve, color )
{
    const cnvs = document . getElementById ( "dragonCanvas" );
    const ctx  = cnvs . getContext ( "2d" );

    // run asynchronous interval
    if ( ! g_wasPaused )
    {
        for ( let i = 0; i < g_intervalCnt; ++i )
            g_intervals . push ( setInterval ( drawIter, 0, curve, color, ctx ) );
    }
}
//------------------------------------------------------------------------------
function flipLR ( lst )
{
    const lstNew = [];
    for ( let i = 0; i < lst . length; ++i )
    {
        if ( lst [i] === 'L' )
            lstNew . push ( 'R' );
        if ( lst [i] === 'R' )
            lstNew . push ( 'L' );
    }
    return lstNew;
}
//------------------------------------------------------------------------------
function dragonRec ( lst, n )
{
    if ( n === 0 )
        return lst;

    lst . push ( 'L' );
    // lst2:
    //      1) take the original list without the last 'L'
    //      2) flip the L/R in this list
    //      3) reverse the order of this list
    //      4) append this list to the original list
    let lst2 = lst . slice ( 0, lst . length - 1 );
    lst2 = flipLR ( lst2 );
    lst2 . reverse ();              // works in place
    lst = lst . concat ( lst2 );    // doesn't work in place

    return dragonRec ( lst, n - 1 )
}
//------------------------------------------------------------------------------
function resetPos ()
{
    g_yPos = g_yInit;
    g_xPos = g_xInit;
    g_curDir = g_initDir;
}
//------------------------------------------------------------------------------
function dragon ( n )
{
    const lst = [];
    return dragonRec ( lst, n );
}
//------------------------------------------------------------------------------
// draw one curve
function draw ( n, color )
{
    const dragonCurve = dragon ( n );
    drawOnCanvasDataIter ( dragonCurve, color );
}
//------------------------------------------------------------------------------
function clearIntervals ()
{
    for ( let i = 0; i < g_intervals . length; ++i )
    {
        clearInterval ( g_intervals [i] );
    }
    g_intervals = [];
}
//------------------------------------------------------------------------------
// restart the whole application
function reset ()
{
    g_i = 0; // current iteration of curve drawing, for animation

    g_yPos = g_yLen / 2;
    g_xPos = g_xLen / 2;

    g_Running = false;

    g_Finished = false;

    g_wasPaused = false;

    g_curDir = g_initDir;

    clearIntervals ();

    const btn = document . getElementById ( "btnStart" );
    btn . innerHTML = "Start";
}
//------------------------------------------------------------------------------
function switchBtnLabel ( id, label1, label2 )
{
    const btn = document . getElementById ( id );
    btn . innerHTML = ( btn . innerHTML === label1 ? label2 : label1 );
}
//------------------------------------------------------------------------------
function switchStartBtnLabel ()
{
    switchBtnLabel ( "btnStart", "Start", "Stop" );
}
//------------------------------------------------------------------------------
function start ()
{
    // if finished => do nothing
    if ( g_Finished )
        return;

    // if running => pause
    if ( g_Running )
    {
        switchStartBtnLabel ();
        g_wasPaused = true;
        g_Running = false;
        console . log ( "Paused" );
        return;
    }

    // if not running => start
    console . log ( "Running..." );
    switchStartBtnLabel ();
    g_Running = true;
    draw ( ITER, MAIN_COLOR );
}
//------------------------------------------------------------------------------
function init ()
{
    createLayout ();
}
//------------------------------------------------------------------------------
